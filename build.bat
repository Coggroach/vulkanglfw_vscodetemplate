@echo off
if exist "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" (
    call "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" x64
) else (
    call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat" x64
)

C:\VulkanSDK\1.1.126.0\Bin\glslangValidator.exe -V -o shaders/frag.spv shaders/depth_shader.frag
C:\VulkanSDK\1.1.126.0\Bin\glslangValidator.exe -V -o shaders/vert.spv shaders/depth_shader.vert

set compilerflags=/Od /Zi /MT /EHsc /std:c++latest /I include /I "C:\VulkanSDK\1.1.126.0\Include"
set linkerflags=/OUT:bin\main.exe
set libFiles=gdi32.lib opengl32.lib kernel32.lib user32.lib shell32.lib msvcrt.lib msvcmrt.lib "C:\VulkanSDK\1.1.126.0\Lib\*.lib"
cl.exe %compilerflags% src\*.cpp lib\*.lib %libFiles% /link %linkerflags%
del bin\*.ilk *.obj *.pdb